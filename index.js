/*
Bai 1:
Input : nhap so ngay lam

step:
s1: tao ra bien chua so ngay
s2: tao ra bien luong
s3: ap dung cong thuc tinh luong vao bien luong

Output : In ra luong
 */

var day=3;
var luong=null;
luong=day*100000;

console.log("luong: ", luong);

/*
Bai 2:
Input : nhap 5 so thuc

step:
s1: tao ra bien chua 5 so thuc
s2: tao ra bien trung binh
s3: ap dung cong thuc tinh bien trung binh

Output : In ra trung binh cong
*/
var a=3 , b=4, c=5, d=6 , e=7;
var tbc=null;
tbc= (a+b+c+d+e)/5;

console.log("tbc:", tbc);

/*
Bai 3
Input: Nhap tien USD

step:
s1: tao ra bien chua USD
s2: tao ra bien VND
s3: ap dung cong thuc tinh VND

Output: In ra VND
 */
var usd=3;
var vnd=null;
vnd= 23500*usd;

console.log("vnd:", vnd)

/*
Bai 4
Input: Nhap chieu dai, chieu rong

step;
s1: tao ra bien chieu dai, chieu rong
s2: tao ra bien dien tich, chu vi
s3: ap dung cong thuc tinh dien tich, chu vi

Output : In ra dien tich, chu vi
*/
var chieuDai=5, chieuRong=3;
var dienTich=null, chuVi=null;
dienTich=chieuDai*chieuRong;
chuVi=(chieuDai+chieuRong)*2;

console.log("dienTich",dienTich,"chuVi",chuVi);

/*
Bai 5
Input: Nhap so

Step:
s1: tao ra bien so
s2: tao ra bien hang don vi, tao ra bien hang chuc
s3: tao ra bien tong 2 ky so va tinh

Output : In ra tong 2 ky so
 */
var so=43;
var so_hang_dv = null;
var so_hang_chuc = null;
so_hang_dv = so%10;
so_hang_chuc = so/10;
var tong=null;

tong= Math.floor(so_hang_chuc) + so_hang_dv;

console.log("tong:",tong);